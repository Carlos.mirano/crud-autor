import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearAutorComponent } from './components/crear-autor/crear-autor.component';
import { EditarAutorComponent } from './components/editar-autor/editar-autor.component';
import { TablaAutoresComponent } from './components/tabla-autores/tabla-autores.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CrearAutorComponent,
    EditarAutorComponent,
    TablaAutoresComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
