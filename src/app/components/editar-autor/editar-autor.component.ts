import { Component, OnInit, Injectable } from '@angular/core';
import { AutorService } from 'src/app/services/autor.service';

@Component({
  selector: 'app-editar-autor',
  templateUrl: './editar-autor.component.html',
  styleUrls: ['./editar-autor.component.css']
})

export class EditarAutorComponent implements OnInit {

  autor: any = {
    id: 0,
    name: ''
  }

  constructor(public edit: AutorService) {
  }

  ngOnInit() {
    this.edit.enviarAutorObservable.subscribe(autor => {
      this.autor = autor;
    });
  }

  enviarAutorEdit() {
    if (this.autor.name !== '') {
      this.edit.editarAutor(this.autor);
      this.autor = {};
    } else {
      alert("Seleccione un autor para editar");
    }
  }

}
