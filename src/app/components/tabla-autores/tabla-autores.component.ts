import { Component, OnInit } from '@angular/core';
import { AutorService } from '../../services/autor.service';

@Component({
  selector: 'app-tabla-autores',
  templateUrl: './tabla-autores.component.html',
  styleUrls: ['./tabla-autores.component.css']
})

export class TablaAutoresComponent implements OnInit {

  autoresT = [];
  autor: any = {
    id: 0,
    name: ''
  }
  constructor(private autorService: AutorService) { }

  ngOnInit() {
    this.autoresT = this.autorService.listarAutores();
    this.autorService.enviarAutorObservable.subscribe(autor =>{
      this.autor = autor;
    });
  }

  enviarAutor(aut) {
    this.autor = aut;
  }

  enviar(aut) {
    this.autorService.enviarAutor(aut);
  }

  eliminar() {
    var index = this.autoresT.indexOf(this.autor);
    this.autorService.eliminarAutor(index);
  }

}
