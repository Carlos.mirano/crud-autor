import { Component, OnInit } from '@angular/core';
import { AutorService } from 'src/app/services/autor.service';
import { Autor } from 'src/app/interfaces/autor';

@Component({
  selector: 'app-crear-autor',
  templateUrl: './crear-autor.component.html',
  styleUrls: ['./crear-autor.component.css']
})
export class CrearAutorComponent implements OnInit {

  autor: any = {
    id: 0,
    name: ''
  }

  constructor(private autorService: AutorService) { }

  ngOnInit() { }

  agregar() {
    if (this.autor.name !== '') {
      this.autor.id = this.autorService.listarAutores().length + 1;
      this.autorService.agregarAutor(this.autor);
      this.autor = { id: 0, name: '' };
    } else {
      alert("El campo de autor está vacío");
    }
  }

}
