import { Injectable } from '@angular/core';
import { Autor } from '../interfaces/autor';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  autores = [
    {
      "id": 1,
      "name": "Homero"
    },
    {
      "id": 2,
      "name": "William Shakespeare"
    },
    {
      "id": 3,
      "name": "Miguel de Cervantes"
    },
    {
      "id": 4,
      "name": "Marcel Proust"
    },
    {
      "id": 5,
      "name": "León Tolstói"
    },
    {
      "id": 6,
      "name": "Gustave Flaubert"
    },
    {
      "id": 7,
      "name": "Johann Wolfgang Von Goethe"
    },
    {
      "id": 8,
      "name": "Gabriel Garcia Márquez"
    },
    {
      "id": 9,
      "name": "Georges Orwell"
    },
    {
      "id": 10,
      "name": "Frank Kafka"
    },
    {
      "id": 11,
      "name": "Antoine De Saint Exupéry"
    },
    {
      "id": 12,
      "name": "Anna Frank"
    },
    {
      "id": 13,
      "name": "Dan Brown"
    },
    {
      "id": 14,
      "name": "John Ronald Reuel Tolkien"
    },
    {
      "id": 15,
      "name": "Fiodor Dostoievski"
    }
  ]

  autor: any = {
    id: 0,
    name: ''
  }

  autorService: any = {};
  private enviarAutorSubject = new Subject<any>();
  enviarAutorObservable = this.enviarAutorSubject.asObservable();

  constructor() { }

  ngOnInit() {
  }

  enviarAutor(autorE: any) {
    this.autorService = autorE;
    this.enviarAutorSubject.next(autorE);
  }

  listarAutores() {
    return this.autores;
  }

  agregarAutor(autor: Autor) {
    this.autores.push(autor);
    this.listarAutores();
  }

  editarAutor(aut) {
    var index = this.autores.indexOf(aut);
    if(index !== -1){
      this.autores.splice(index, 1, aut);
    } else {
      alert("Seleccione un autor para editar");
    }
  }

  eliminarAutor(id) {
    this.autores.splice(id, 1);
    this.listarAutores();
  }

}
