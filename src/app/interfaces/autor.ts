export interface Autor {
    id: number;
    name: string;
}
